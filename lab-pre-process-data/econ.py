import pandas as pd
def getTouristThailand():
  url = 'https://gitlab.com/tapattan/machine-learning/-/raw/main/datasources/International-Tourist-Arrivals_to_Thailand.csv'
  df = pd.read_csv(url)
  df = df.set_index('No')
  return df


def getRegisNewCar(year,url=''):
  if(url==''):    
    url = 'https://gitlab.com/tapattan/machine-learning/-/raw/main/datasources/n_whole.xls' 
  
  year = str(year) 
  xl = pd.ExcelFile(url)
  sn = xl.sheet_names
  if(not year in sn):
    return 0,'ไม่พบข้อมูล'  
  
  month_name = ['January','February', 'March', 'April',
            'May', 'June', 'July', 'August',
            'September', 'October', 'November','December']
  
  df = pd.read_excel(url,sheet_name=year,skiprows=[0,1])
  df = df.T 
  df = df[df.columns[0:1]]
  df['Year'] = year
  df = df.tail(12)
  df['Month'] = month_name
  df['no'] = range(1,13)
  df = df[['no','Year','Month',0]]
  df = df.set_index('no')
  df.columns = ['Year','Month','Whole']
  return df